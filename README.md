# Assignment 2 AI GT

# THE BERKELEY PROJECT

## Overview

### Application

Create an application featuring a webpage displaying the message "This is the <x> visitor," where <x> is a counter fetched from Redis. The counter should increment with each visit to the index page. The application should be dockerized and provide the link on GitHub.

### CI/CD Pipeline

Develop a plan for the automated deployment of the above application to production using GitLab. Document each stage in detail.

## CDS Application

- Created an app using Flask to track the number of people visiting the app homepage and display the count on the webpage.
- Dockerized the application and deployed it in Kubernetes.
- Set up Redis in the Kubernetes cluster to store the 'visitor_count' value.
- Used a StatefulSet configuration for Redis to allow persistent storage of the visitor tracking data.
- Stored Redis configuration details as a ConfigMap in Kubernetes. Used the ConfigMap to set the environment variables 'REDIS_HOST' and 'REDIS_PORT' in the application container to be used by 'app.py'.
- Set up a Kubernetes Ingress resource to allow access to the app webpage from the application load balancer's public DNS name.

## Application CI/CD Pipeline

I have designed the CI/CD pipeline to work for the dev and main branches in my GitLab repository. These branches deploy to the 'dev-eks' and 'prod-eks' AWS EKS clusters, respectively. Feature branches merged into the dev branch will trigger the dev pipeline.

### Dev CI/CD pipeline:

**Stages:** `build_push_dev` --> `deploy_dev`

**Stage: `build_push_dev`**
The dev pipeline will build and push the Docker image to my Docker Hub registry with the git commit ID and latest tags.

**Stage: `deploy_dev`**
It will then apply the Kubernetes manifests files and pull the latest Docker image with the "dev-<commit-id>" tag into my AWS EKS cluster 'dev-eks'. When you raise a pull request to merge dev to the main branch, the production pipeline will be triggered.

### Prod CI/CD pipeline:

**Stages:** `tag_push_prod` --> `deploy_prod`

**Stage: `tag_push_prod`**
The prod CI/CD pipeline will pull the latest Docker image, which is currently being used and tested in the dev EKS cluster 'dev-eks'. The image pulled is then tagged with a new commit ID from the main branch and pushed to the Docker Hub registry.

**Stage: `deploy_prod`**
The manifest files are applied to the AWS EKS 'prod-eks'. This includes the Redis server, CDS app, services, ConfigMap, and Ingress. Then the image tagged with the "prod-<commit-id>" will be pulled into the EKS cluster 'prod-eks'.

We can add testing stages in the GitLab CI/CD pipeline like CAST, DAST, container image scanning, etc., before deploying the image in the cluster to enhance security.

## Installation

**Prerequisites:**

Before running the scripts, ensure you have the following tools installed and configured:

- kubectl: Kubernetes command-line tool
- Docker Desktop: With Kubernetes enabled
- Docker Hub account: With access from your terminal

**Installation:**

I have created a bash script to set up everything locally on Docker Desktop. Before running the script, change the working directory inside the script. Also, change the service 'cds-visitor-tracker-service' spec.type to 'NodePort' to access the app webpage in your browser. e.g., localhost:<nodeport>

```bash
chmod +x setup_script.sh
./setup_script.sh
```

## Visuals

Please refer to the screenshots provided in the screenshots directory, showing the various stages of this CDS Technical Challenge - Devops test.
