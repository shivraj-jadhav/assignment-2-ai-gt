from flask import Flask, render_template
import os
import redis

app = Flask(__name__)

# Get Redis connection details from environment variables
redis_host = os.environ.get("REDIS_HOST", "localhost")
redis_port = int(os.environ.get("REDIS_PORT", 6379))

try:
    # Connect to Redis
    redis_client = redis.Redis(host=redis_host, port=redis_port)
except redis.exceptions.ConnectionError:
    # Handle connection error gracefully (e.g., log the error)
    print("Error connecting to Redis server")
    redis_client = None  # Set to None to indicate unavailable Redis

@app.route("/")
def index():
    counter = 0

    # Attempt to retrieve counter from Redis (if available)
    if redis_client:
        try:
            # Get the current count from Redis
            counter = int(redis_client.get("visitor_count") or 0)
        except redis.exceptions.ConnectionError:
            # Handle potential errors during retrieval (e.g., log the error)
            print("Error retrieving counter from Redis")

        # Increment the counter in Redis
        redis_client.incr("visitor_count")

    # Render template with counter value
    return render_template("index.html", counter=counter)

if __name__ == "__main__":
    app.run(debug=True)
